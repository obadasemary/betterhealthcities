//
//  ViewController.swift
//  BetterHealthCities
//
//  Created by Abdelrahman Mohamed on 10/18/18.
//  Copyright © 2018 Abdelrahman Mohamed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Parchment

class ViewController: UIViewController, NVActivityIndicatorViewable {
    
    // MARK: - shared
    
    static let shared = ViewController()
    
    // MARK: - @IBOutlet
    
    @IBOutlet weak var searchController: UISearchBar!
    @IBOutlet weak var containerPage: UIView!
    
    // MARK: - init
    
    var pagingViewController: FixedPagingViewController!
    
    let sizeNVActivityIndicator = CGSize(width: 30, height:30)
    
//    var cities = [City]()
    
    var myData: JSONData?
    
    // MARK: - viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getDataFromFile(path: "city10")
        
        setupUIConfige()
    }
    
    // MARK: - setupPagingViewController
    
    func setupPagingViewController() {
        
        pagingViewController.selectedTextColor = .black
        pagingViewController.indicatorColor = .black
        pagingViewController.selectedBackgroundColor = .white
        pagingViewController.menuBackgroundColor = .clear
        pagingViewController.backgroundColor = .white
        pagingViewController.textColor = .black
        pagingViewController.menuItemSpacing = 8
        
        addChildViewController(pagingViewController)
        containerPage.addSubview(pagingViewController.view)
        containerPage.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParentViewController: self)
        pagingViewController.view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setupUIConfige() {
        
        let statesVC = StatesViewController()
        statesVC.title = "States".localized()
        
        let citiesVC = CitiesViewController()
        citiesVC.title = "Cities".localized()
        
        self.pagingViewController = FixedPagingViewController(viewControllers: [
            statesVC,
            citiesVC
            ])
        
        self.setupPagingViewController()
    }
    
    func getDataFromFile(path: String) {
        
        DispatchQueue.main.async {
            
            let path = Bundle.main.path(forResource: path, ofType: "json")
            
            let url = URL(fileURLWithPath: path ?? "")
            do {
                let data = try Data(contentsOf: url)
                self.myData = try JSONDecoder().decode(JSONData.self, from: data)
            } catch let err {
                print(err)
            }
            
            let totalCount = self.myData?.count
            print("totalCount", totalCount ?? 0)
            
            self.myData?.forEach({
                print("State", $0[10])
                print("City", $0[11])
                print($0[12])
            })
            
            //            if let path = Bundle.main.path(forResource: "city17", ofType: "json") {
            //                do {
            //                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            //                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: [])
            ////                    let json = try JSONSerialization.data(withJSONObject: jsonResult)
            ////                    let decodedCountries = try JSONDecoder().decode(ArrayData.self, from: json)
            ////
            ////                    decodedCountries.forEach({
            ////                        $0.forEach({
            ////                            switch $0 {
            ////                            case .integer(let number):
            ////                                print("number", number)
            ////                            case .string(let city):
            ////                                print("city", city)
            ////                            }
            ////                        })
            ////                    })
            //
            //                    if let jsonResult = jsonResult as? [Any] {
            //                        // do stuff
            //                        let totalCount = data.count
            //                        print("totalCount", totalCount)
            //
            //                        print((jsonResult[500] as? [Any])?[10] as Any)
            //
            //                        for i in jsonResult {
            //                            for x in (i as? [Any])! {
            //                                (x as? [Any])?.forEach({
            //                                    print($0)
            //                                })
            //                            }
            //                        }
            //
            //
            ////                        print(jsonResult.last)
            ////                        print("city17", jsonResult[0])
            ////
            ////
            ////                        jsonResult.forEach() { print($0) }
            //
            //                    }
            //                } catch {
            //                    // handle error
            //                    print("handle error", error.localizedDescription)
            //                }
            //            }
        }
    }
    
    func loadJsonFileAndSperateIt() {
        
        DispatchQueue.main.async {
            
            if let path = Bundle.main.path(forResource: "rows", ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                    if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let data = jsonResult["data"] as? [Any] {
                        // do stuff
                        let totalCount = data.count
                        let perPage = Int(totalCount / 500) //Total files count
                        for i in 0...perPage {
                            //For each file get 1000 record
                            var filesArray = [Any]()
                            for j in 0...500 {
                                let record = data[j]
                                filesArray.append(record)
                            }
                            guard let documentDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
                            let fileUrl = documentDirectoryURL.appendingPathComponent("city\(i).json")
                            print(fileUrl.absoluteString)
                            do {
                                let data = try JSONSerialization.data(withJSONObject: filesArray, options: [])
                                try data.write(to: fileUrl)
                            } catch {
                                print("Error")
                            }
                        }
                    }
                } catch {
                    // handle error
                    print("handle error", error.localizedDescription)
                }
            }
        }
    }
    
//    func loadJson(filename fileName: String) -> [PurpleCity]? {
//        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
//            do {
//                let data = try Data(contentsOf: url)
////                let json = try JSONSerialization.jsonObject(with: data, options: [])
////                if let object = json as? [String: Any] {
////                    // json is a dictionary
////                    print(object)
////                } else if let object = json as? [Any] {
////                    // json is an array city10
////                    print(object)
////                } else {
////                    print("JSON is invalid")
////                }
//                let decoder = JSONDecoder()
//                let cities = try decoder.decode([PurpleCity].self, from: data)
//                return cities
//            } catch {
//                print("error:\(error.localizedDescription)")
//            }
//        }
//        return nil
//    }
}

typealias JSONData = [[PurpleJSONDatum]]

enum PurpleJSONDatum: Codable {
    case integer(Int)
    case string(String)
    case unionArray([JSONDatumJSONDatum])
    case null
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode([JSONDatumJSONDatum].self) {
            self = .unionArray(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if container.decodeNil() {
            self = .null
            return
        }
        throw DecodingError.typeMismatch(PurpleJSONDatum.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for PurpleJSONDatum"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        case .unionArray(let x):
            try container.encode(x)
        case .null:
            try container.encodeNil()
        }
    }
}

enum JSONDatumJSONDatum: Codable {
    case bool(Bool)
    case string(String)
    case null
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Bool.self) {
            self = .bool(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if container.decodeNil() {
            self = .null
            return
        }
        throw DecodingError.typeMismatch(JSONDatumJSONDatum.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONDatumJSONDatum"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        case .null:
            try container.encodeNil()
        }
    }
}

////typealias Cities = [City]
//
//struct City: Codable {
//    let category, categoryid, dataValue, dataValueType: String?
//    let dataValueUnit, datasource, datavaluetypeid, geographiclevel: String?
//    let highConfidenceLimit, lowConfidenceLimit, measure, measureid: String?
//    let population2010, shortQuestionText, stateabbr, statedesc: String?
//    let uniqueid, year, cityfips, cityname: String?
//    let dataValueFootnote, dataValueFootnoteSymbol, geolocation, tractfips: String?
//
//    enum CodingKeys: String, CodingKey {
//        case category, categoryid
//        case dataValue = "data_value"
//        case dataValueType = "data_value_type"
//        case dataValueUnit = "data_value_unit"
//        case datasource, datavaluetypeid, geographiclevel
//        case highConfidenceLimit = "high_confidence_limit"
//        case lowConfidenceLimit = "low_confidence_limit"
//        case measure, measureid, population2010
//        case shortQuestionText = "short_question_text"
//        case stateabbr, statedesc, uniqueid, year, cityfips, cityname
//        case dataValueFootnote = "data_value_footnote"
//        case dataValueFootnoteSymbol = "data_value_footnote_symbol"
//        case geolocation, tractfips
//    }
//}
//
//enum PurpleCity: Codable {
//    case integer(Int)
//    case string(String)
//    case unionArray([CityCity])
//    case null
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(Int.self) {
//            self = .integer(x)
//            return
//        }
//        if let x = try? container.decode([CityCity].self) {
//            self = .unionArray(x)
//            return
//        }
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        if container.decodeNil() {
//            self = .null
//            return
//        }
//        throw DecodingError.typeMismatch(PurpleCity.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for PurpleCity"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .integer(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        case .unionArray(let x):
//            try container.encode(x)
//        case .null:
//            try container.encodeNil()
//        }
//    }
//}
//
//enum CityCity: Codable {
//    case bool(Bool)
//    case string(String)
//    case null
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(Bool.self) {
//            self = .bool(x)
//            return
//        }
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        if container.decodeNil() {
//            self = .null
//            return
//        }
//        throw DecodingError.typeMismatch(CityCity.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for CityCity"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .bool(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        case .null:
//            try container.encodeNil()
//        }
//    }
//}
//
//typealias ArrayData = [[ArrayDatum]]
//
//enum ArrayDatum: Codable {
//    case integer(Int)
//    case string(String)
//
//    init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if let x = try? container.decode(Int.self) {
//            self = .integer(x)
//            return
//        }
//        if let x = try? container.decode(String.self) {
//            self = .string(x)
//            return
//        }
//        throw DecodingError.typeMismatch(ArrayDatum.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ArrayDatum"))
//    }
//
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        switch self {
//        case .integer(let x):
//            try container.encode(x)
//        case .string(let x):
//            try container.encode(x)
//        }
//    }
//}


